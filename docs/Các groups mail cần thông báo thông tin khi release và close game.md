#### Các groups mail cần thông báo thông tin khi release và close game.

October 11, 2019 [General](https://wiki.mto.zing.vn/category/pqa/general/),
[GT-PQA](https://wiki.mto.zing.vn/category/pqa/)

1.  **Release game**

Sau khi System Engineer (SE) nhận được thông tin/mail thông báo về việc
release/OB game, SE cần gửi thông tin đó đến các team liên quan để các bên cùng
nắm thông tin và sắp xếp hỗ trợ khi sản phẩm release/OB thông qua các groups
mail.

Groups mail:

1.  gamesupport\@vng.com.vn : bao gồm các team PASS, BA, FA, IAAS, IFRS,
    GT-Payment.

2.  dataservice\@vng.com.vn

3.  gt.framework\@vng.com.vn: GT-Framework team.

4.  gt.pqa\@vng.com.vn: PQA team.

5.  tg.servicedesk\@vng.com.vn

6.  SE leader.

7.  Các cá nhân hay groups mail khác (nếu có).

**2. Close game**

Sau khi System Engineer (SE) nhận được thông tin/mail thông báo về việc đóng
game, SE ngoài tạo change close game ticket trên ITSM tool (*Hướng dẫn tạo và
quản lý change close game ticket trên ITSM tool)* thì cần gửi email thông báo
việc đóng game đến các team liên quan thông qua các groups mail.

Groups mail:

1.  gamesupport\@vng.com.vn : bao gồm các team PASS, BA, FA, IAAS, IFRS,
    GT-Payment.

2.  dataservice\@vng.com.vn

3.  gt.framework\@vng.com.vn: GT-Framework team.

4.  gt.pqa\@vng.com.vn: PQA team.

5.  tg.servicedesk\@vng.com.vn

6.  SE leader.

7.  Các cá nhân hay groups mail khác (nếu có).

Note: Đối với những game chạy trên platform 360game thì ngoài các groups mail
trên còn cần gửi thông tin đóng game tới **DuyNK4, DangTGH và CC thêm SoaNT**
