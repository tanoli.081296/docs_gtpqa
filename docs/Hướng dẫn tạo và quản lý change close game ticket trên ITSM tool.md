#### Hướng dẫn tạo và quản lý change close game ticket trên ITSM tool

October 11, 2019 [General](https://wiki.mto.zing.vn/category/pqa/general/),
[GT-PQA](https://wiki.mto.zing.vn/category/pqa/)

#### Policies

Đối với SE:

1.  Mở change trên ITSM trước 2 ngày làm việc tính từ thời điểm phải đóng
    payment.

2.  Chịu trách nhiệm các thông tin trên change đúng và đủ (ngày start-end,
    tasklist,…)

3.  **Kiểm tra tất cả các task đã được hoàn thành trước khi tiến hành đóng
    change.**

Đối với các team cung cấp dịch vụ hạ tầng (DC, IaaS, SDK, SnS,…)

1.  Cung cấp contact point nếu có thay đổi cho SDK.

2.  Chịu trách nhiệm thực hiện task theo kế hoạch được SE tạo trong ITSM

Đối với PQA:

1.  Theo dõi chi phí hạ tầng và thông báo các team liên quan xử lý khi có issue.

2.  Cập nhật contact point của GT (SE, Payment, Passport, Promotion, Telco) cho
    team SDK khi có thay đổi.

Trong trường hợp phát sinh chi phí sau thời gian Change đã đóng, department của
người owner task chịu trách nhiệm về các phát sinh này.

#### Tổng quát quy trình tạo change

a. Sau khi nhận được thông báo đóng game từ Product team, SE gửi/forward mail
thông tin cho các team liên quan qua các groups mail ([Các groups mail cần thông
báo thông tin khi release và close
game](https://wiki.mto.zing.vn/2019/10/11/cac-groups-mail-can-thong-bao-thong-tin-release-close-san-pham/))

b.Trong vòng 2 ngày trước ngày chính thức đóng payment, SE tạo close game ticket
trên ITSM.

1.  Leader cần approve trước ngày đóng payment.

2.  Sau khi leader approve, tool tự động gửi mail task đến các assignee.

c. SE, Assignees của các team hạ tầng thực hiện task theo đúng thời gian của
từng task.

d. Sau khi tất cả các task hoàn thành (Done/Cancel), SE check lại tất cả nội
dung công việc đã hoàn thành đúng chưa?

1.  Hoàn thành đúng: Close Passed

2.  Chưa hoàn thành nhưng status của task là “Done”/”Cancel” : Close Failed, nêu
    rõ lí do.

e. Assignee/PQA kiểm tra thông tin phí peering, CDN

f. Nếu có phát sinh, thông báo cho team cung cấp dịch vụ xử lí và chịu trách
nhiệm.

#### Danh sách các tasklist.

![](media/fb4022f30262eeea934db877c8a0fee9.png)

#### Hướng dẫn tạo “Close game” ticket trên ITSM

Link:
[https://itsmv2.vng.com.vn/](https://itsmv2.vng.com.vn/change/add_close_product)

![](media/3a4f4c8a7efc43c63ef67d98fe90828b.png)

Vào link tool ITSM, sau đó click chọn **[Close Game]** button

![](media/3ace6f658bc4eb09d0fb66a19e063d40.png)

Điền thông tin game đóng, bao gồm: **Product name**; **Final Closing Date**:
Ngày đóng game hoàn toàn; **Start date**: Ngày đóng payment; **End date** =
Final closing date + 30 ngày (nếu End date trùng vào ngày lễ, t7, chủ nhật thì
end date sẽ được delay sang ngày làm việc đầu tiên sau đó).

![](media/f279bfc161ab4724ea8fd69d055ad604.png)

Sau khi điền đẩy đủ thông tin, chọn **[Save change]** button và **[Continue
Edit]**

![](media/210e0c850cb8ba9aa927c1dd1373f964.png)

![](media/2015c4953278f9b9143eb9e5d5096e61.png)

Sau khi save change, **SE có thể review và edit tasklis**t (remove, add, edit
date,…)

![](media/e47f7c54aee56ec1ff8226fd94d7194c.png)

Sau khi hoàn tất chỉnh sửa, chọn **[Submit]** button, **[OK]** để submit change.

Lúc này status của change là : **Waiting for Approval**

![](media/072617c764d9621a78eb956b23cb75ac.png)

SE Leader có vai trò review change: **[Approve]** – Đồng ý với tất cả nội dung
trong change, **[Return]** – Change cần chỉnh sửa nội dung, return lại để SE
chỉnh sửa, **[Reject]** – Change có vấn đề hoặc không thực hiện change nữa.

![](media/23fbf1a605338233e88d076a41996889.png)

Sau khi change được approve bởi SE leader (**status của change là
In-progress**), SE owner/Assignee có thể vào tab **\<My-Todo\>** để xem những
change ID do mình phụ trách.

![](media/357d8b9b5ff6568b54b295af1e3d06ae.png)

– SE owner/Assignee follow và **thực hiện task do mình phụ trách đúng thời gian
(chú ý đến thời gian đóng game hoàn toàn, để tránh gây ra incident)**.

– Sau khi hoàn thành task trên các tool thực thi của từng bộ phận, SE
owner/Assignee điền thông tin: Actual start, Actual end, Note (nếu có), Action
(Save, Cancel, Done).

![](media/535e8537d1faf8e6ccb3d621cbc1505b.png)

Sau khi tất các các task trong tasklist được các Assignee cập nhật trạng thái
(Done/Cancel),…

![](media/e56cab4d2d973760e1b079db7aba2828.png)

– SE owner tiến hành đóng change ticket bằng cách điền thời gian thực tế thực
hiện change vào Actual time (Start, end, downtime start – nếu có, downtime end –
nếu có), sau đó chọn [Done] button. Lúc này status của change chuyển sang
**Waiting for verification**.

– SE owner tiếp tục chọn: **[Pass] button** – Nếu change được thực hiện thành
công, hoàn thành đúng thời gian lên kế hoạch từ đầu (planned time).

Ngược lại, **[Fail] button** – Nếu change thực hiện quá thời gian lên kế hoach
từ đầu. Và trong trường hợp chọn fail, SE owner cần điền lí do change fail.

Change ở trạng thái Closed là hoàn thành việc update change trên ITSM tool.

Lưu ý: Sau khi game đã được đóng hoàn toàn, Assignee và PQA sẽ theo dõi chi phí
phát sinh của game vào đầu tháng tiếp theo để thông báo cho team cung cấp dịch
vụ xử lí và chịu trách nhiệm.
